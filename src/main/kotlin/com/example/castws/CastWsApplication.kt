package com.example.castws

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CastWsApplication

fun main(args: Array<String>) {
	runApplication<CastWsApplication>(*args)
}
