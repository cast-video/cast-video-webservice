package com.example.castws.dto

/**
 * Video inside the playlist.
 */
data class Video(
        override var id: String,
        override var title: String,
        override var duration: Int): IVideo
