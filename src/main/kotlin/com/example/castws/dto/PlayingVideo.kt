package com.example.castws.dto

/**
 * Playing video.
 */
data class PlayingVideo(
        override var id: String,
        override var title: String,
        override var duration: Int,
        var playing: Boolean): IVideo
