package com.example.castws.dto

data class CastState(val playing: PlayingVideo, val playlist: List<Video>)
