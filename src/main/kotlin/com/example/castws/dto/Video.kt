package com.example.castws.dto

interface IVideo {
    /** YouTube identifier of the video. */
    var id: String

    /** Title of the video. */
    var title: String

    /** Video duration inside seconds. */
    var duration: Int
}
