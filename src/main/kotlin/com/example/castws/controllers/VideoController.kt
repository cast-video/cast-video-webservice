package com.example.castws.controllers

import com.example.castws.services.VideoService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessageSendingOperations
import org.springframework.stereotype.Controller

@Controller()
class VideoController {

    companion object {

        const val MAIN_TOPIC = "/topic/cast-data"

        const val FETCH_DATA = "/fetch"

        const val ADD_TO_PLAYLIST = "/playlist"
    }

    @Autowired
    private lateinit var service: VideoService

    @Autowired
    private lateinit var messaging: SimpMessageSendingOperations

    /**
     * The user wants to fetch the data at any time.
     */
    @MessageMapping(FETCH_DATA)
    @SendTo(MAIN_TOPIC)
    fun fetch() = service.fetch()

    /**
     * The user adds a video to the playlist.
     */
    @MessageMapping(ADD_TO_PLAYLIST)
    fun addToPlaylist(@Payload id: String) {

        service.addToPlaylist(id)

        broadcast()
    }

    /** Broadcasts the state of the video player. */
    private fun broadcast() = messaging.convertAndSend(MAIN_TOPIC, service.fetch())
}
