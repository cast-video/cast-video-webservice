package com.example.castws.services

import com.example.castws.dto.CastState
import com.example.castws.dto.PlayingVideo
import com.example.castws.dto.Video
import org.springframework.stereotype.Service

@Service
class VideoService {

    fun fetch(): CastState {
        // TODO: Fetch data from database.

        val playing = PlayingVideo("DipEiYAyKxY", "Hoshi - Ta Marinière (Clip Officiel)", 5000, false)

        return CastState(playing, listOf(
                Video("37StRy0LEbI", "OrelSan - La pluie (feat. Stromae) [CLIP OFFICIEL]", 1000),
                Video("MFhKnRLEihE", "Calogero - Face à la mer ft. Passi", 1000)
        ))
    }

    /**
     * Adds the video to the playlist.
     */
    fun addToPlaylist(videoID: String) {

    }
}
